server '178.63.67.157', user: 'decidim', roles: %w{app db web}, port: 22224
set :branch, proc { `git rev-parse --abbrev-ref preproduction`.chomp }
set :rails_env, "production"
