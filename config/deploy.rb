# config valid for current version and patch releases of Capistrano
lock "~> 3.14.1"

set :application, "giro2020"
set :repo_url, "git@gitlab.com:ciudadaniai/giro2020.git"

# restart app by running: touch tmp/restart.txt
# at server machine
# set :passenger_restart_with_touch, true

# Deploy to the user's home directory
# set :deploy_to, "/home/deploy/#{fetch :application}"
set :deploy_to, "/home/decidim/decidim-app/"

append :linked_dirs, 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', '.bundle', 'public/system', 'public/uploads'
append :linked_files, 'config/application.yml'

# Only keep the last 5 releases to save disk space
set :keep_releases, 5
